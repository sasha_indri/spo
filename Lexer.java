package spo;


import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Lexer {
	List<Token> tokens = new ArrayList<Token>();

    String accum="";
    private int dl=0;
    private boolean sbit=false;
    private int numst=0;

    private Pattern digitPattern = Pattern.compile("^0|[1-9]{1}[0-9]*$"); //��������� �������� 

    private Pattern varPattern = Pattern.compile("^[a-zA-Z]+$");

    private Pattern smPattern = Pattern.compile("^;$");

    private Pattern assignOpPattern = Pattern.compile("^=$");

    private Pattern addOpPattern = Pattern.compile("^\\+$");

    private Pattern decOpPattern = Pattern.compile("^\\-$");

    private Pattern wsPattern = Pattern.compile("^\\s*$");

    private Map<String, Pattern> commonTerminals = new HashMap<String, Pattern>();  // �������� ���-�������

    private String currentLucky = null;

    private int i;

    public Lexer(){
        commonTerminals.put("DIGIT", digitPattern); // ��������� �������� � �� ������������ �� ����  DIGIT : ^0|[1-9]{1}[0-9]*$ �� ���� ���������� ������� ������������  
        commonTerminals.put("VAR", varPattern);
        commonTerminals.put("SM", smPattern);
        commonTerminals.put("ASSIGN_OP", assignOpPattern);
        commonTerminals.put("ASSIGN_ADD", addOpPattern);
        commonTerminals.put("ASSIGN_DEC", decOpPattern);
        commonTerminals.put("WS", wsPattern);
    }
    public void processInput(String fileName) throws IOException {

        File file = new File(fileName);   // ������ �� ����� ����� 
        Reader reader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(reader);
        String line;
        while( (( line = bufferedReader.readLine() ) != null)&&!sbit ) {
        	dl = line.length();
            processLine(line);
            numst++;
        }
        System.out.println("TOKEN("
                + currentLucky
                + ") recognized with value : "
                + accum
        );

        tokens.add(new Token(currentLucky, accum));

        for (Token token: tokens) {
            System.out.println(token);
        }

    }

    private void processLine(String line) {

        for (i=0; i<line.length(); i++) {
            accum = accum + line.charAt(i); // ��������� ������ �� ������� � �� ������ � ����� 
            processAccum(i);
            
        }
        
    }

    private void processAccum(int k) {
        boolean found = false;
        boolean f1=false, f2=false;
        for ( String regExpName : commonTerminals.keySet() ) {  // ��������� ������ �� ���_������� 
            Pattern currentPattern = commonTerminals.get(regExpName); // ��������� �������� ����� 
            Matcher m = currentPattern.matcher(accum); // ��������� ������� ������������ �� ��������� ����� ���. �� �������
            if(dl-1==k){
            	f1=true;
            	//System.out.println("����� ��� ");
            	dl=0;
            }
            if( m.matches() ) { // � ������� ���������� 
                currentLucky = regExpName; // �������� ���� 
                found = true; // ������ ��������, ������ ������ 
               
               
                
            } else {
            	//System.out.println("������ "+);
            }
        }
        if (currentLucky != null && !found){  // ����� � ������ ��������� ����������� 
        	
            System.out.println("TOKEN("
                    + currentLucky
                    + ") recognized with value : "
                    + accum.substring(0,accum.length()-1)
                    
            );
            f2=true;
            tokens.add( new Token( currentLucky, accum.substring(0,accum.length()-1 ) ) ); // ������� ����� � �������� ��� � ������ �������
            i--;
            accum = "";
            currentLucky = null;
        }
        else{
        	//System.out.println("�� ���������� "+accum);
        	if(f1&&!f2){
        		System.out.println("������ �� ��������� ������"+accum+" ������> "+numst+"\n");
        		sbit=true;
        		return ;
        	}
        }

    }

    public List<Token> getTokens() {
        return tokens;
    }
}
