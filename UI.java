package spo;
import java.io.IOException;
import java.util.List;


public class UI {
	

	static UtilsHelper utilsHelper = new UtilsHelper();

    public static void main(String[] args) throws IOException {

       validTest();	 // ������ ���������

}

    public static void validTest() throws IOException { 
        utilsHelper.printFile("src/valid.input");   // ����� �� ������ ����������� ����� 
        process("src/valid.input");       
    }
    
    public static void inValidTest() throws IOException {
        utilsHelper.printFile("src/invalid.input");
        process("src/invalid.input");
    }
    
    public static void process(String fileName) throws IOException {
        Lexer lexer = new Lexer();   // �������� �. ������ ������
        lexer.processInput(fileName); // �������� ������ 
        List<Token> tokens = lexer.getTokens(); // �������� ������ ������� �� ������� 
        Parser parser = new Parser(); // ������ ������
        parser.setTokens(tokens); // ����� ���������� ������� 
        parser.lang();  // ����� ��������� ������� 
    }
    
}